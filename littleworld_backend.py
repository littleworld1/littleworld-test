"""
back-end for little world
"""
from __future__ import print_function

import sqlite3
import random
from threading import Timer
from flask import Flask, jsonify, Response, request, redirect
from flask_restful import abort

app = Flask(__name__)

# Database for the world
conn = sqlite3.connect('database/dataworld.db', check_same_thread=False,
                       detect_types=sqlite3.PARSE_DECLTYPES)
db = conn.cursor()


class Person:
    """
    class for Person - resident of the little world
    """
    id_world = 0
    id_person = 0
    colour = ''
    x_person = 0
    y_person = 0
    direction = ''
    number_steps = 0

    def __init__(self):
        """
        create table for people
        """
        datapeople = "CREATE TABLE IF NOT EXISTS People (id_world INTEGER," \
                     "id_person INTEGER, colour TEXT, x FLOAT," \
                     "y FLOAT, direction TEXT, number_steps INTEGER)"
        db.execute(datapeople)
        conn.commit()

    # we need x_lentgth, y_width to make the world "infinite"
    # we need an array of people to avoid several people in one place
    def step(self, direction, x_length, y_width, people):
        """
        first, person turns in right direction;
        then makes a step forward
        """
        self.turn(direction)
        x_coord = self.x_person
        y_coord = self.y_person
        if direction == 'right':
            x_new = self.x_person + 1
            x_coord = x_new
            if x_new > x_length:
                x_coord -= x_length
        elif direction == 'up':
            y_new = self.y_person + 1
            y_coord = y_new
            if y_new > y_width:
                y_coord -= y_width
        elif direction == 'left':
            x_new = self.x_person - 1
            x_coord = x_new
            if x_new < 0:
                x_coord += x_length
        else:
            y_new = self.y_person - 1
            y_coord = y_new
            if y_new < 0:
                y_coord += y_width

        for person_step in people:
            if person_step.x_person == x_coord and \
               person_step.y_person == y_coord:
                # another direction
                directions = ['right', 'up', 'left', 'down']
                directions.remove(direction)
                new_direction = random.choice(directions)
                # print("\t\t\tCHANGED direction from " + direction + " to "
                #       + new_direction)
                self.step(new_direction, x_length, y_width, people)
                return
        self.x_person = x_coord
        self.y_person = y_coord
        self.number_steps += 1
        db.execute('UPDATE People SET x = ?, y = ?, number_steps = ? where'
                   '(id_person = ?)',
                   (self.x_person, self.y_person, self.number_steps,
                    self.id_person))
        conn.commit()
        # print("\t\tstep " + str(self.number_steps) + " " + str(self.x) + "/"
        #       + str(self.y))

    def turn(self, direction):
        """
        person turns before making a step
        """
        self.direction = direction
        db.execute('UPDATE People SET direction = ? where (id_person = ?)',
                   (direction, self.id_person))
        conn.commit()


class World:
    """
    class for the little world
    """
    # class attributes (data)
    id = 0
    age = 0
    x_length = 0
    y_width = 0
    number_people = 0
    number_red = 0
    number_blue = 0
    people = []

    def __init__(self):
        """
        create a table for the world;
        create new world or take the one that already exists
        """
        # create table
        dataworld = "CREATE TABLE IF NOT EXISTS World (id INTEGER," \
                    "age INTEGER, x FLOAT, y FLOAT," \
                    "number_people INTEGER, number_red INTEGER," \
                    "number_blue INTEGER)"
        db.execute(dataworld)
        conn.commit()
        db.execute('SELECT * FROM World WHERE (id IS ?)', (self.id,))
        # create new world
        if db.fetchone() is None:
            db.execute('INSERT INTO World (id, age, x, y, number_people,'
                       'number_red, number_blue) VALUES'
                       '(?, ?, ?, ?, ?, ?, ?)', (self.id, self.age,
                                                 self.x_length, self.y_width,
                                                 self.number_people,
                                                 self.number_red,
                                                 self.number_blue))
            conn.commit()
        # or take the one that already exists
        else:
            row = db.execute('SELECT * FROM World WHERE (id IS ?)', (self.id,))
            self.age = row.fetchone()[1]
            row = db.execute('SELECT * FROM World WHERE (id IS ?)', (self.id,))
            self.x_length = row.fetchone()[2]
            row = db.execute('SELECT * FROM World WHERE (id IS ?)', (self.id,))
            self.y_width = row.fetchone()[3]
            row = db.execute('SELECT * FROM World WHERE (id IS ?)', (self.id,))
            self.number_people = row.fetchone()[4]
            row = db.execute('SELECT * FROM World WHERE (id IS ?)', (self.id,))
            self.number_red = row.fetchone()[5]
            row = db.execute('SELECT * FROM World WHERE (id IS ?)', (self.id,))
            self.number_blue = row.fetchone()[6]
            db.execute("SELECT count(name) FROM sqlite_master WHERE \
                       type='table' AND name='People'")
            if db.fetchone()[0] == 1:
                all_people = db.execute('SELECT * FROM People WHERE id_world \
                                        IS ?', (self.id,)).fetchall()
                for existing_person in all_people:
                    new_person = Person()
                    new_person.id_world = existing_person[0]
                    new_person.id_person = existing_person[1]
                    new_person.colour = existing_person[2]
                    new_person.x_person = existing_person[3]
                    new_person.y_person = existing_person[4]
                    new_person.direction = existing_person[5]
                    new_person.number_steps = existing_person[6]
                    self.people.append(new_person)

    def create_world(self, x_from_user, y_from_user):
        """
        set parameters of the world and populate it
        """
        self.set_xy(int(x_from_user), int(y_from_user))
        self.populate(2)

    def delete(self):
        """
        delete the world
        """
        self.age = 0
        self.x_length = 0
        self.y_width = 0
        self.number_people = 0
        self.number_red = 0
        self.number_blue = 0
        self.people.clear()
        db.execute('UPDATE World SET age = ?, x = ?, y = ?, number_people = ?,'
                   'number_red = ?, number_blue = ? WHERE (id = ?)',
                   (0, 0, 0, 0, 0, 0, self.id))
        conn.commit()
        db.execute("SELECT count(name) FROM sqlite_master WHERE \
                               type='table' AND name='People'")
        if db.fetchone()[0] == 1:
            db.execute('DELETE FROM People WHERE (id_world = ?)', (self.id,))
            conn.commit()

    def set_xy(self, x_length, y_width):
        """
        set parameters of the world
        """
        self.x_length = x_length
        self.y_width = y_width
        db.execute('UPDATE World SET x = ?, y = ? WHERE (id = ?)',
                   (self.x_length, self.y_width, self.id))
        conn.commit()

    # class methods (behaviour)
    def populate(self, number_people):
        """
        populate the world - create people
        """
        self.number_people = number_people
        self.number_red = self.number_people // 2
        self.number_blue = self.number_people - self.number_red
        db.execute('UPDATE World SET number_people = ?, number_red = ?,'
                   'number_blue = ?'
                   'where (id = ?)', (self.number_people, self.number_red,
                                      self.number_blue, self.id))
        conn.commit()
        id_person = -1
        for i in range(0, number_people):
            person = Person()
            person.id_world = self.id
            id_person += 1
            person.id_person = id_person
            if i < self.number_red:
                person.colour = 'red'
            else:
                person.colour = 'blue'
            person.x_person = float(random.randint(0, self.x_length))
            person.y_person = float(random.randint(0, self.y_width))
            person.direction = random.choice(['right', 'up', 'left', 'down'])
            self.people.append(person)
            db.execute('INSERT INTO People (id_world, id_person, colour, x, y,'
                       'direction, number_steps) VALUES'
                       '(?, ?, ?, ?, ?, ?, ?)', (person.id_world,
                                                 person.id_person,
                                                 person.colour,
                                                 person.x_person,
                                                 person.y_person,
                                                 person.direction,
                                                 person.number_steps))
            conn.commit()

    def move_people(self):
        """
        each person of the world makes a step
        """
        self.age += 1
        db.execute('UPDATE World SET age = ? where (id = ?)', (self.age,
                                                               self.id))
        conn.commit()
        # print("\nage " + str(self.age))
        for person_move in self.people:
            # print("\tinitial direction " + person.direction)
            direction = random.choice(['right', 'up', 'left', 'down'])
            # print("\tdirection " + direction)
            # print("\tinitial coord " + str(person.x) + "/" + str(person.y))
            person_move.step(direction, self.x_length, self.y_width,
                             self.people)
            # print("\tfinal coord " + str(person.x) + "/" + str(person.y) +
            #       "\n")


class RepeatingTimer:
    """
    class for the timer that stops and restarts
    """

    def __init__(self, interval, function, *args, **kwargs):
        """
        create a timer
        """
        self.interval = interval
        self.function = function
        self.args = args
        self.kwargs = kwargs
        self.timer = None

    def callback(self):
        """
        callback
        """
        self.function(*self.args, **self.kwargs)
        self.start()

    def cancel(self):
        """
        cancel the timer
        """
        self.timer.cancel()

    def start(self):
        """
        start the timer
        """
        self.timer = Timer(self.interval, self.callback)
        self.timer.start()


# instance of a class = object (экземпляр = объект)
my_world = World()

t = RepeatingTimer(1, my_world.move_people)
t.start()


def get_people_data():
    """
    get data about every person from the world
    """

    people_data = []
    for resident in my_world.people:
        data = {"id_world": resident.id_world, "id_person": resident.id_person,
                "colour": resident.colour, "x": resident.x_person,
                "y": resident.y_person, "direction": resident.direction,
                "number_steps": resident.number_steps}
        people_data.append(data)
    return people_data


# API
@app.route('/api/v1/world', methods=['GET'])
def world_data():
    """
    all the data about the world is displayed in
    json format on http://127.0.0.1:5000/api/v1/world
    """

    return jsonify({"id": my_world.id, "age": my_world.age, "x":
                    my_world.x_length, "y": my_world.y_width,
                    "number_of_people": {"all": my_world.number_people,
                                         "red": my_world.number_red,
                                         "blue": my_world.number_blue},
                    "people": get_people_data()})


@app.route('/setx_y', methods=['POST'])
def set_xy():
    """
    get parameters of the world from users
    """
    x_post = request.form['x']
    y_post = request.form['y']
    if int(x_post) > 0 and int(y_post) > 0:
        t.cancel()
        my_world.delete()
        my_world.create_world(x_post, y_post)
        t.start()
        return redirect("http://127.0.0.1:5000/api/v1/world")
    return abort(400)


@app.route('/health', methods=['GET'])
def health():
    """
    health
    """

    return Response("OK", status=200)


app.run()
