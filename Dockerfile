FROM python:3.9.6-slim-buster

WORKDIR /usr/src/app

COPY requirements.txt ./
RUN pip install --no-cache-dir -r requirements.txt

COPY . .

RUN pwd && \
    ls -la

CMD [ "python", "./littleworld-backend.py" ]
