import random
import threading

# API
from flask import Flask, jsonify
from flask_restful import Api
app = Flask(__name__)
api = Api(app)

class Person:
    colour = ''
    x = 0
    y = 0
    direction = ''
    number_steps = 0

    # we need x_lentgth, y_width to make the world "infinite"
    # we need an array of people to avoid several people in one place
    # we need age to stop the recursion when we change the direction of a person
    def step(self, direction, x_length, y_width, people, age):
        # first, person turns in right direction
        self.turn(direction)
        x = self.x
        y = self.y
        if direction == 'right':
            x_new = self.x + 1
            x, y = self.x, self.y
            if x_new > x_length:
                x = x_new - x_length
            else:
                x = x_new
        elif direction == 'up':
            y_new = self.y + 1
            if y_new > y_width:
                y = y_new - y_width
            else:
                y = y_new
        elif direction == 'left':
            x_new = self.x - 1
            if x_new < 0:
                x = x_new + x_length
            else:
                x = x_new
        else:
            y_new = self.y - 1
            if y_new < 0:
                y = y_new + y_width
            else:
                y = y_new

        for person in people:
            if person.x == x and person.y == y:
                # another direction
                directions = ['right', 'up', 'left', 'down']
                directions.remove(direction)
                new_direction = random.choice(directions)
                print("\t\t\tCHANGED direction from " + direction + " to " + new_direction)
                self.step(new_direction, x_length, y_width, people, age)
        # stop recursion
        if self.number_steps == age:
            return
        self.x = x
        self.y = y
        self.number_steps += 1
        print("\t\tstep " + str(self.number_steps) + " " + str(self.x) + "/" + str(self.y))

    def turn(self, direction):
        self.direction = direction

class World:
    # class attributes (data)
    x_length = 0
    y_width = 0
    people = []
    age = 0
    number_people = 0
    number_red = 0
    number_blue = 0

    def setXY(self, x, y):
        if x <= 0 or y <= 0:
            print('x, y should be positive')
            self.setXY(int(input()), int(input()))
        self.x_length = x
        self.y_width = y

    # class methods (behaviour)
    def populate(self, max_number):
        if max_number < 2:
            print('Maximum number of people should be not less than 2')
            self.populate(int(input()))
            return
        number_people = random.randint(2, max_number)
        self.number_people = number_people
        number_red = random.randint(0, number_people)
        self.number_red = number_red
        self.number_blue = self.number_people - self.number_red
        for i in range(0, number_people):
            person = Person()
            if i < number_red:
                person.colour = 'red'
            else:
                person.colour = 'blue'
            person.x = random.randint(0, self.x_length)
            person.y = random.randint(0, self.y_width)
            person.direction = random.choice(['right', 'up', 'left', 'down'])
            self.people.append(person)

    def move_people(self, period):
        if period <= 0:
            print('Age length should be positive')
            self.move_people(float(input()))
        T = threading.Timer(period, my_world.move_people, args=[period])
        T.start()
        self.age += 1
        print("\nage " + str(self.age))
        for person in self.people:
            print("\tinitial direction " + person.direction)
            direction = random.choice(['right', 'up', 'left', 'down'])
            print("\tdirection " + direction)
            print("\tinitial coord " + str(person.x) + "/" + str(person.y))
            person.step(direction, self.x_length, self.y_width, self.people, self.age)
            print("\tfinal coord " + str(person.x) + "/" + str(person.y) + "\n")

# instance of a class = object (экземпляр = объект)
my_world = World()

print('Enter world parameters x and y')
my_world.setXY(int(input()), int(input()))

print('Enter maximum number of people in the world')
my_world.populate(int(input()))

print('Enter age length of your world in seconds')
my_world.move_people(float(input()))

# API
@app.route('/api/v1/world', methods=['GET'])
def age():
    return jsonify({"age": my_world.age, "x": my_world.x_length, "y": my_world.y_width,
                    "number_of_people": {"all": my_world.number_people, "red": my_world.number_red,
                                "blue": my_world.number_blue}})

app.run()